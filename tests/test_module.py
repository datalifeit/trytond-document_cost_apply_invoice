# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class DocumentCostApplyInvoiceTestCase(ModuleTestCase):
    """Test Document Cost Apply Invoice module"""
    module = 'document_cost_apply_invoice'


del ModuleTestCase
